package Homework_1128;

public class Movie extends Culture {

	String gerne;
	
	public Movie(String title, int director, int actor, String gerne) {
		super(title, director, actor);
		this.gerne = gerne;
		// TODO Auto-generated constructor stub
	}

//	2) Movie 클래스를 정의한다.
//	- 추가로 장르를 필드로 가지며 생성자를 이용하여 값을 초기화한다.
//	- 메소드는 다음과 같이 정의한다.
//	(1) public abstract void getInformation() : 제목, 참여감독 수, 참여배우 수, 관객수, 총점, 평점을 출력하는 기능

	@Override
	public void getInformation() {
		System.out.println("영화제목 : " + title);
		System.out.println("감독 : " + director);
		System.out.println("배우 : " + actor);
		System.out.println("영화총점 : " + total);
		System.out.println("영화평점 : " + getGrade());
		System.out.println("================");
	}
	
	
}
