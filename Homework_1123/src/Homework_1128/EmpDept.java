package Homework_1128;

public class EmpDept extends Employee {
	
	public String departmentName;
	
	
	public EmpDept(String name, int salary, String departmentName) {
		super(name, salary); // 부모클래스객체생성
		this.departmentName = departmentName;
	}


	public String getDepartmentName() {
		return departmentName;
	}


	@Override
	public void getlnformation() {
		super.getlnformation();
		System.out.println("  부서 : " + departmentName);
		
	}

	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
}
