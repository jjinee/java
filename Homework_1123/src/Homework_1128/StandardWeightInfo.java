package Homework_1128;

public class StandardWeightInfo extends Human {

	

	public StandardWeightInfo(String name, double height, double weight) {
		super(name, height, weight);
		
	}

	@Override
	public void getInformation() {
		super.getInformation();
		//1번 방식
		//System.out.print( " 표준체중 : " + getStandardWeight());
		//2번 방식
		System.out.printf("표준체중 %.1f 입니다 .\n", getStandardWeight());
	}

	public double getStandardWeight() {
		//return (double)(this.height-100)*0.9;
		double sw =(height-100)*0.9;
		return sw;
	}
	
}
