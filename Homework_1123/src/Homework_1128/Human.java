package Homework_1128;

public class Human {
	
	String name;
	double height;
	double weight;
	
	public Human (String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	
	
	public void getInformation() {
		System.out.println(name + "님의 신장 " + height + ", 몸무게 " + weight);
	}
	
}
