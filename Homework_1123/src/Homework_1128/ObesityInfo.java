package Homework_1128;

public class ObesityInfo extends StandardWeightInfo {

	
	public ObesityInfo(String name, double height, double weight) {
		super(name, height, weight);
		
	}


	@Override
	public void getInformation() {
		String obesity = null;
		if(getObesity() > 24 && getObesity() < 31 ) {
			obesity = "비만입니다.";
		} else if (getObesity() > 30) {
			obesity = "고도비만입니다.";
		} else { 
			obesity = "정상입니다";
		}
		System.out.println(name + "님의 신장 " + height + ", 몸무게 " + weight + ", " + obesity);
	}

	
	public double getObesity() {
		
		double bmi = (weight - getStandardWeight()) / getStandardWeight() *100;
		return bmi;
		//return (weight - getStandardWeight()) / getStandardWeight() *100;
	}
	
	

}
