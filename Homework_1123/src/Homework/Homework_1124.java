package Homework;

import java.util.Scanner;

public class Homework_1124 {
	public static void main(String[] args) {
		
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서
		// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
		// 1) 메뉴는 다음과 같이 구성하세요.
		// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
		// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
		//  -> 배열써서 Scanner 사용해 입력. 상품 클래스 만들 때 필드로는 상품명과 가격 들어가야
		//  입력 -> 반복문 활용 (배열의 크기만큼 반복)
		// 3) 제품별 가격을 출력하세요.
		//	출력예시, "상품명 : 가격"
	        //  출력 -> 반복문 활용 ( 배열의 크기만큼) -> 각 방에 있는 객체 하나씩 꺼내옴
			//  객체가 가지 고있는 필드(정보 담은 변수) -> 출력 예시에 맞게 만듦
		// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
		// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.
			// 반복문 종료 직전 프로그램 종료한다는 말 출력
				
		
		
		
		Scanner sc = new Scanner(System.in);
		int goodsCount = 0;
		Goods[] goodsAry = null;
	
		// while 말고 switch 문 쓸때는 boolean flag = true. 종료할때  false로 바꿔서
		while (true)  {
			System.out.println("1. 상품 수 | 2. 상품 및 가격 입력 | 3. 제품별 가격 | 4. 분석 | 5. 종료" );
			System.out.println("입력>");
			int selectNo = Integer.parseInt(sc.nextLine());
			// == String selectNo = sc.nextLing();
			
			// = if(selectNo.equals("1");
			if(selectNo == 1) { // 배열의 크기만 받아야 함
				System.out.println("상품 수 입력>"); 
				goodsCount = Integer.parseInt(sc.nextLine());
		
			} else if(selectNo == 2) { 
				// 데이터 넣을 때 배열의 크기를 확정
				goodsAry = new Goods[goodsCount];
				
				for(int i = 0; i < goodsAry.length; i++) {
					// 상품 객체 생성
					// 반복문 안에 넣는 이유 : 한바퀴 돌 때 마다 초기화 -> 새 객체 계속 만들어짐
					Goods gd = new Goods(); 
							
		
				System.out.println("상품명>");
				// 변수에 데이터 입력 받고 객체에 넣는 방법
				String name = sc.nextLine();
				gd.name = name;
		
		
				System.out.println("상품가격>");
				// 데이터 입력 받음과 동시에 객체에 넣어주는 방법
				gd.price = Integer.parseInt(sc.nextLine());
				
				goodsAry[i] = gd;
				System.out.println();
				}
				
			} else if (selectNo == 3) {
				// 반복문 활용 (배열의 크기만큼) -> 각 방에 있는 객체 하나씩 꺼내옴
				for(int i=0; i < goodsAry.length; i++) {
					System.out.println(goodsAry[i].name + " : " + goodsAry[i].price);			
				}
				
			} else if (selectNo == 4) {
				String maxGoods = null;
				int max = 0; //max = goodAry[i] 	
				int sum = 0;
				
				for(int i = 0; i < goodsAry.length; i++) {
					if(max < goodsAry[i].price) {
					   max = goodsAry[i].price;
					   maxGoods = goodsAry[i].name;
					} 
				}
				
				for(int i = 0; i < goodsAry.length; i++) {
					if(goodsAry[i].name != maxGoods) {
					   sum = sum + goodsAry[i].price;
					}
				}
				
				// 반복문 한 번 만 쓰는 법 : 모든 제품의 합 - 최고가격 	
//				for(int i = 0; i < goodsAry.length; i++) {
//					if(max < goodsAry[i].price) {
//					   max = goodsAry[i].price;
//					}
//					//상품들 가격 합계 구하기
//					sum += goodsAry[i].price;
//				}
//			 	System.out.println("최고가 상품 : " + max);
//			 	System.out.println("최고가 상품을 제외한 상품들의 가격 합 : " + (sum-max));
				
				 System.out.println("최고가 상품 : " + maxGoods);
				 System.out.println("최고가 상품 가격 : " + max);
				 System.out.println("최고가 상품을 제외한 상품들의 가격 합 : " + sum);
				 
				 
			} else if(selectNo == 5) {
				System.out.println("프로그램 종료");
				break;
			}	
	
		}
	}
	
}
