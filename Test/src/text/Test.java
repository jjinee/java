package text;

import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
		//1. 주사위 크기 2. 주사위 굴리기 3. 결과 보기 4, 가장 많이 나온 수 5. 종료
	
	int[] dice = null;
	int size = 0;
	
	boolean run = true;
	
	while(run) {
		System.out.println("=== 1.주사위 크기 2.주사위 굴리기 3.결과 보기 4. 가장 많이 나온 수 5.종료 ===");
		System.out.println("메뉴 >");
		String selectNo = sc.nextLine();
		
		switch (selectNo) {
		case "1":
			System.out.println("주사위 크키 >");
			size = Integer.parseInt(sc.nextLine());
			if(size < 5 || size > 10) {
				System.out.println("입력한 값의 범위를 벗어 났습니다. 5~10 사이의 수를 입력해주세요");
			}
			break;
		case "2":
			dice = new int[size];
			int count = 0;
			//5가 나올 때 까지 -> 언제까지 진행할지 조건을 알수가 없
			while(true) {
				int random = (int)(Math.random()* size) + 1;
				// 각 숫자 나온 횟수 저장 
				// -1 한 이유 = 랜덤값 4 = 배열에서 3번째 인덱스
				dice[random-1] = dice[random-1] + 1;
				count++;
				if(random == 5) {
					break;
				}
			}
			System.out.println("5가 나올 때까지 주사위를 " + count + "번 굴렸습니다.");
			break;
		case "3":
			for(int i = 0; i < dice.length; i++) {
				System.out.println((i+1) + "은 " + dice[i] + "번 나왔습니다.");
			}
			break;
		case "4":
			int max = 0;
			int index = 0;
			for(int num : dice) {
				if(max < num) {
					max = num;
				}
			}
			for(int i = 0; i <dice.length; i++) {
				if(max <dice[i]) {
					index = i;
				}
			}
			System.out.println("가장 많이 나온 수는" + (index+1) + "입니다.");
			break;
		case "5":
			System.out.println("프로그램 종료");
			run = false;
		break;

			
		}
		
	}
	
	
	
	
	
	
	
	
	}
}
