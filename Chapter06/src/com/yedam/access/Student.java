package com.yedam.access;

public class Student {

	//필드
		private String stdName;
		private String major;
		private String stdGrade;
		private int programing;
		private int dataBase;
		private int OS;
		
		
		//생성자
		public Student() {
					
		}
		// 클래스를 통한 객체를 생성할 때 첫번째로 수행하는 일들을 모아두는 곳
		// 필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
		// 생성자에서 this 키워드 활용해서 필드 초기화 하면 됨
				
				
		
		//메소드
		public String getStdName() {
			return stdName;
		}




		public void setStdName(String stdName) {
			this.stdName = stdName;
		}




		public String getMajor() {
			return major;
		}




		public void setMajor(String major) {
			this.major = major;
		}




		public String getStdGrade() {
			return stdGrade;
		}




		public void setStdGrade(String stdGrade) {
			this.stdGrade = stdGrade;
		}




		public int getPrograming() {
			return programing;
		}




		public void setPrograming(int programing) {
			if(programing <= 0) {
				this.programing = 0;
			}
			this.programing = programing;
		}




		public int getDataBase() {
			return dataBase;
		}




		public void setDataBase(int dataBase) {
			this.dataBase = dataBase;
		}




		public int getOS() {
			return OS;
		}




		public void setOS(int oS) {
			OS = oS;
		}




		
		
		
	
	
}
