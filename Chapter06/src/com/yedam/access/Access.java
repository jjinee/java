package com.yedam.access;

public class Access {
	// 필드
	/*
	/* public 어디서는 누구나 다 접근 가능
	 * protected 상속받은 상태에서 부모-자식간에 사용(패키지 달라도 사용 가능)
	             같은 패키지내 클래스 사용
	 * default 같은 패키지에서만 사용 가능
	 * private 내가 속한 클래스에서만 사용 가능
	 */
	
	// 접근제한자 -> 이름 지어서 사용한 곳 다 가능(변수, 클래스, 메소드 등등). 생성자 메소드도 가능
	
	public String free;
	protected String parent;
	private String privacy; // -> 캡슐화 : 정보 은닉
	String basic;  // default
	
	
	
	
	// 생성자 ctrl + 스페이스바
	public Access() {
		
	} 
	
	private Access(String privacy) {
		this.privacy = privacy;
	} // 노란줄 : 경고. 못쓰는데 왜만드냐ㅑㅑㅑㅑ
		
	
	
	
	
	//메소드
	//public void run() {
	//	System.out.println("달립니다");
	//}
	
	public void free() {
		System.out.println("접근이 가능합니다.");
		 privacy();
	}
	
	private void privacy() {
		System.out.println("접근이 불가능합니다.");
	}
	// privacy 쓰는법 public에 넣음 (내부에서는 쓸 수 있기때문에)
	// 굳이 이렇게 쓰는 이유 : 회사 기술 숨길 때
	
	
	
	
}
