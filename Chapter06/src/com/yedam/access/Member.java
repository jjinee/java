package com.yedam.access;

public class Member {
	
	// 필드
	private String id;
	private String pw;
	private String name;
	private int age;
	
	
	// 생성자
	
	
	
	// 메소드
	// setter, getter -> 데이터의 무결성 지키기 위해서
	// 매개변수 통해서 데이터 넣음
	public void setAge(int age) {
		if(age < 0 ) {
			System.out.println("잘못된 나이입니다.");
			return; // return; -> return;을 만나면 하던 것을 멈추고
			        // 메소드를 종료한 후. 메소드 호출한 곳으로 이동.
					// void 에도 사용 가능
		} else {
			this.age = age;
		}
		this.age = age;
	}
	
	public int getAge() {
		// 미국 나이와 한국 나이 한살차이 나므로 아래 내용을 실행
		 age = age + 1;
		 return age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		if(id.length() < 8) {
			System.out.println("8글자 미만입니다. 다시 입력해주세요");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
