package com.yedam.access;

public class Singleton {

	
	// 정적 필드
	// 단 하나의 객체를 생성 (생성자 private로 막아놨기 때문에 나자신에서 객체 하나 만듦 (아무도 접근 못하는))
	private static Singleton singleton = new Singleton();
	
	
	// 생성자
	private Singleton() { //외부에서 만드는거 차단
		
	}
	
	// 정적 메소드
	public static Singleton getInstance() {
		return singleton;
	}
	
}
