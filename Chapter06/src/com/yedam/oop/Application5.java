package com.yedam.oop;

import com.yedam.access.Access;

public class Application5 {
	public static void main(String[] args) {
		
		Access access = new Access();
		// 클래스의 접근제한
		
		// public
		access.free = "free";
		
		// protected
		//access.parent = "parent";
		
		// private
		//access.privacy ="privacy";
		
		//default
		//access.basic = "basic";
	
		
		
	}
}
