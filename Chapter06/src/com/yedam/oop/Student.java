package com.yedam.oop;

public class Student {
	
	// 필드
	String name;
	String school = "예담고등학교";
	int number;
	int kor;
	int math;
	int eng;
	
	
	// 생성자
	public Student(String name, String school, int number, int kor, int math, int eng) {
		this.name = name;
		this.number = number;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
	}
	
	public Student() {		
		
	}
	
	
	// 메소드
	void getInfo() {
		System.out.println("학교의 이름 : " + name);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + number);
		System.out.println("총 점 : " + sum());
		System.out.println("평 균 : " + avg());
	}
	
	int sum() {
		return kor + eng + math;
	}
	
	double avg() {
		double avgs = sum() / (double)3;
		return avgs;
	}

	
}
