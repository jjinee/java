package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		//설계문 토대로 객체 만들고 동작시킴
		
		// SmartPhone 클래스(설계도)를 토대로 iphone14Pro 만듦
		// 클래스 변수 = new 생성자
		// SmartPhone iphone14Pro = new SmartPhone(); //기본생성자
		
		SmartPhone iphone14Pro = new SmartPhone("Apple", "iphone14Pro", 500 ); // 매개변수 들어간 생성자
		
		// . : 설계도에서 만든 필드, 메소드 쓸 수 있게함 -> 정보 등록
//		iphone14Pro.maker = "Apple";
//		iphone14Pro.name = "iphone14Pro";
//		iphone14Pro.price = 100000;		
//		iphone14Pro.price = 500; // 변경 가능

//		매개변수 들어간 생성자 통하면 바로 정보 넣으면서 물건 만들기 때문에 생략 가능
		
		//iphone14Pro.call();
		iphone14Pro.hangUp();
		
		//필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		
		// SmartPhone 클래스(설계도) 하나로 다양한 객체 만들기 가능
		// 설계도 복사해서 물건 만듦 - 만든 정보 물건에 담김. 설계도 원본은 그대로 유지
		SmartPhone zflip4 = new SmartPhone("samsung", "zflip4", 10000);
		zflip4.maker = "samsung";
		zflip4.name = "zflip4";
		zflip4.price = 10000;
		
		//zflip4.call();
		zflip4.hangUp();
		
		
		
		
		
		SmartPhone sony = new SmartPhone();
		
		//리턴 타입이 없는 메소드
		// int a = sony.getInfo(0); // 리턴값 없는데 int a 가 데이터 달라해서 오류
		sony.getInfo(0);
		
		//리턴 타입이 int 메소드
		int b = sony.getInfo("int"); // 0 가져옴 -> int b = 0
		System.out.println(b);
		
		//리턴 타입이 String[] 메소드
		String[] temp = sony.getInfo(args);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
