package com.yedam.oop;

public class ConstantNo {
	
//	//필드
	
	
	//final 수정할 수 없는 필드
//	final double pi = 3.14123123;
//	final int earthRound = 46250;
//	final int lightSpeed = 127000;
	
	//static final 상수 불변 공용
	
	static final double PI = 3.14123123;
	static final int EARTH_ROUND = 46250;
	static final int LIGHT_SPEED = 127000;
	
	// 생성자
	public ConstantNo() {
//		this.pi = 10.0; 오류. final로 정의된 것 못바꿈
	} 
//	
	
}
