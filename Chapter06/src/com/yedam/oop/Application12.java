package com.yedam.oop;

import java.util.Scanner;

public class Application12 {
	
	int speed;
	
	void run() {
		System.out.println(speed + "으로 달립니다.");
	}
	
	// 스태틱 영역에 등록된 친구들
	public static void main(String[] args) {
		
		// int speed2 =speed; // 스태틱 영역 아니라서 사용 x
		// run();
		// 사용법
		
//		Application12 app new Application12();
//		app.speed = 5;
//		app.run();
		
		Scanner sc = new Scanner(System.in);
		
		
		Car myCar = new Car("포르쉐");
		Car yourCar = new Car("벤츠");
		
		myCar.run();
		yourCar.run();
		
		
		// 정적 필드, 메소드 부르는 방법
		// 정적 멤버가 속해 있는 클래스명.필드 or 메소드 명
		// 1) 정적 필드 가져오는 방법
		double prRatio = Calculator.pi;
		System.out.println(prRatio);
		//System.out.println(Calculator.pi);
		
		// 2) 정적 메소드 가져오는 방법
		//System.out.println(Calculator.plus(5, 6));
		int result = Calculator.plus(5, 6);
		System.out.println(result);
		
		
		// 특징
		// 1) ☆★☆★모든 클래스에서 가져와서 사용할 수 있음 -> 공유의 개념
		// 2) ☆★☆★너무 남용해서 사용하면 메모리 누수(부족) 현상 발생할 수 있음.		
		// 3) ☆★주의할 점★☆
		//    정적 메소드에서 외부에 정의한 필드, 메소드를 사용하려고 한다면
		//    static이 붙은 필드 또는 메소드만 사용 가능
		//    해당 필드와 메소드가 속해 있는 클래스를 인스턴스화 하여서
		//    인스턴스 필드와 인스턴스 매소드를 dot(.) 연산자를 통해 가져와서 사용.
		
		
		// Person - final 필드
		
		Person p1 = new Person("123123-123456", "김또치");
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		 // p1.nation ="USA"; -> 오류. final로 정의된 것 못바꿈
		
		
		// static final
		System.out.println(5*5*ConstantNo.PI);
		System.out.println(ConstantNo.EARTH_ROUND);
		
		
		
		
	}
}
