package com.yedam.oop;

public class Book {

	// 필드
	String name;
	String type = "학습서";
	String price;
	String publisher;
	String isbn;
		
	
	
	
	// 생성자
	public Book(String name, String price, String publisher, String isbn) {
		
		this.name = name;
		this.price = price;
		this.publisher = publisher;
		this.isbn = isbn;
	}
	
	// 메소드
	
	void getInfo() {

		System.out.println("책 이름 : " + name);
		System.out.println("1) 종류 : " + type);
		System.out.println("2) 가격 : " + price);
		System.out.println("3) 출판사 : " + publisher);
		System.out.println("4) 도서번호 : " + isbn);
		
	}
	
}
