package com.yedam.oop;

public class Application4 {
 public static void main(String[] args) {
	Student stud1 = new Student("고길동", "예담고", 221124, 80, 90, 60);
	
	Student stud2 = new Student();
	stud2.name = "김둘리";
	stud2.number = 221125;
	stud2.kor = 100;
	stud2.math = 60;
	stud2.eng = 90;
			
	Student stud3 = new Student("김또치", "예담고", 221126, 70, 60, 95);
	
	stud1.getInfo();
	System.out.println();
	stud2.getInfo();
	System.out.println();
	stud3.getInfo();
	
	
	
 }
}

