package com.yedam.oop;

public class SmartPhone {
	// 설계도 만들기
	
	
	//필드 
	//객체의 정보를 저장
	String name; // 하나의 정보 계속 유지해서 사용할 때 String name = iphone14Pro; 바로 데이터 넣어줄수 있음
	String maker;
	int price;
	
	
	// 생성자
	// 객체 초기화
	// 클래스랑 이름 같아야
	// 자바에서 생성자가 클래스 내부에 "하나도 없을 때" 알아서 기본 생성자를 만들고 객체 생성
	
	// 기본생성자
	public SmartPhone() { 
		
	} 
	
	// 매개변수가 들어간 생성자
	public SmartPhone(String name, String maker, int price){ // 매개변수. 밖에서 가져오는 데이터. 필드의 name maker price와 다름. 구분하기 위해 this 사용. this = 내자신. 클래스 내부에 있는..
		this.name = name;
		this.maker = maker;
		this.price = price;
		call(); // <- 메소드 호출
	}
	
	// 매개변수 하나인 생성사
	public SmartPhone(String name) {
		//객체 만들 때 원하는 행동 또는 데이터 저장 등등
	}
	
	// 매개변수 두개인 생성자
	public SmartPhone(String name, int price) {
		
	}
	
	// 생성자 이름 같아도 뒤에 매개변수의 타입, 개수, 순서가다르면 다르게 인식 - 생성자 오버로딩
	
	
	//메소드
	//객체의 기능을 정의
	void call() {
		System.out.println(name + " 전화를 겁니다.");
	}
	void hangUp() {
		System.out.println(name + " 전화를 끊습니다.");
	}
	
	
	
	// 1) 리턴 타입이 없는 경우 : void 
	void getInfo(int no) {
		System.out.println("매개변수 : " + no);
	}
	
	// 2) 리턴 타입이 있는 경우
	//   1. 기본타입 : int, double, long....
	//   2. 참조타입 : String, 배열, 클래스....

	// 리턴 타입이 기본 타입일 경우
	int getInfo(String temp ) {
	
		return 0;
	}
	
	// 리턴 타입이 참조 타입일 경우
	String[] getInfo(String[] temp) {
		
		return temp; //스트링 타입의 배열 반환해야
	}
	
	
	
}
