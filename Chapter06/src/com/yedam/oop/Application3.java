package com.yedam.oop;

public class Application3 {
	public static void main(String[] args) {
		
		Book Java = new Book("혼자 공부하는 자바", "24000원", "한빛미디어", "yedam-001");
		
		Book Linux = new Book("이것이 리눅스다", "32000원", "한빛미디어", "yedam-002");
		
		Book JavaScript = new Book("자바스크립트 파워북", "22000원", "어포스트", "yedam-003");
		
	
		
		Java.getInfo();
		System.out.println();
		Linux.getInfo();
		System.out.println();
		JavaScript.getInfo();
		

	}
}
