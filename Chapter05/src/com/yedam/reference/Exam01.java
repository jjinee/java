package com.yedam.reference;

public class Exam01 {
	public static void main(String[] args) {
		int intVal = 10;
		int[] array = {1, 2, 3, 4, 5, 6};
		int[] array2 = {1, 2, 3, 4, 5, 6};
		int[] array3; // 데이터X -> 주소X => null 가짐 -NullPointException 오류
		System.out.println(intVal); // 기본타입 바로 데이터
		System.out.println(array);  // 참조타입 heap에 있는 주소값
		System.out.println(array2);
		System.out.println(array == array2); // == 데이터 주소값 비교 -> false
		
		// memory leak - > 메모리 누수, 메모리 부족
		
	}
}
