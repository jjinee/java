package com.yedam.reference;

public class Exam02 {
	public static void main(String[] args) {
		
		String strVal1 = "yedam";
		String strVal2 = "yedam";
		
		
		// 참조 타입의 == 데이터가 있는 메모리 주소 비교
		if(strVal1 == strVal2) {
			System.out.println("strVal1과 strVal2의 메모리 주소는 같다.");
		} else {
			System.out.println("strVal1과 strVal2의 메모리 주소는 다르다.");
		}
		
		
		// 문자열(String)간의 데이터 비교
		if(strVal1.equals(strVal2)) {
			System.out.println("strVal1과 strVal2의 데이터는 같다.");
		} else {
			System.out.println("strVal1과 strVal2의 데이터는 다르다.");
		}

		
	
		// new 연산자
		String strVal3 = new String("yedam");
		String strVal4 = new String("yedam");

		
		if(strVal3 == strVal4) {
			System.out.println("strVal3과 strVal4의 메모리 주소는 같다.");
		} else {
			System.out.println("strVal3과 strVal4의 메모리 주소는 다르다.");
		}
		
		
		if(strVal3.equals(strVal4)) {
			System.out.println("strVal3과 strVal4의 데이터는 같다.");
		} else {
			System.out.println("strVal3과 strVal4의 데이터는 다르다.");
		}
		
		
		if(strVal1 == strVal3) {
			System.out.println("strVal1과 strVal3의 메모리 주소는 같다.");
		} else {
			System.out.println("strVal1과 strVal3의 메모리 주소는 다르다.");
		}
		
		if(strVal1.equals(strVal3)) {
			System.out.println("strVal1과 strVal3의 데이터는 같다.");
		} else {
			System.out.println("strVal1과 strVal3의 데이터는 다르다.");
		}
		
	}
}
