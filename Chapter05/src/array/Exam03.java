package array;

import java.util.Scanner;

public class Exam03 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int[] ary;
		int no;
		System.out.println("배열의 크기>");
		no = Integer.parseInt(sc.nextLine());
		
		ary = new int[no];
		
		System.out.println(ary.length);
		
		for(int i=0; i<ary.length; i++) {
			System.out.println("입력>");
			ary[i] = Integer.parseInt(sc.nextLine());
		}
		
		System.out.println(ary[0]);
		System.out.println(ary[1]);
		System.out.println(ary[2]);
	}
}
