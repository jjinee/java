package array;

import java.util.Scanner;

public class Exam05 {
	public static void main(String[] args) {
		
		boolean run = true;
		int studentNum = 0;
		int[] scores = null;
		Scanner sc = new Scanner(System.in);
		
		while(run) {
			System.out.println("--------------------------------------------------");
			System.out.println("1. 학생수 | 2. 점수입력 | 3. 점수리스트 | 4. 분석 | 5. 종료");
			System.out.println("--------------------------------------------------");
			System.out.println("선택> ");
			
			int selectNo = Integer.parseInt(sc.nextLine());
			
			if(selectNo == 1) {
				// 배열의 크기 (방의 개수) 지정
				System.out.println("학생수>");
				studentNum = Integer.parseInt(sc.nextLine());
				// 배열 크기 설정 완료 
				// 오류났을때 확실히 하기위해
				//scores = new int[studentNum];// 오류났을때 확실히 하기위해 2번에서
				
			} else if(selectNo == 2) {
				scores = new int[studentNum];
				for(int i = 0; i < scores.length; i++) {
					System.out.println("scores["+i+"]>"); 
					scores[i] = Integer.parseInt(sc.nextLine());
				}
		
			} else if(selectNo == 3) {
				for(int i = 0; i < scores.length; i++) {
				System.out.println("scores["+i+"]>" + scores[i]);	
				}
				
			} else if(selectNo == 4) {
				int sum = 0;
				int max =scores[0];
				
				for(int i = 0; i<scores.length; i++) {
					if(max < scores[i]) {
						max = scores[i];
					}
					sum = sum + scores[i];
				}	
				System.out.println("최고점수 : " + max);
				System.out.println("평균 점수 : " + sum /scores.length );
			
//				int max = 0;
//				for(int i = 0;  i<scores.length; i++) {
//					if(max < scores[i]) {
//						max = scores[i];
//					}
//				}
//				System.out.println("최고점수 : " + max);
//				
//				int sum = 0;
//				for (int i = 0; i < scores.length; i++) {
//					sum = sum + scores[i];
//				}
//				
//				double avg = (double)sum / scores.length; 
//				
//				System.out.println("평균 점수 : " + avg);
				
			} else if(selectNo == 5) {
				run = false;			
			}			
		}
		System.out.println("프로그램 종료");
		
		
		
		
	}
}
