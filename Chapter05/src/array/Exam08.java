package array;

public class Exam08 {
	public static void main(String[] args) {
		String[] strArray = new String[3];
		
		//String str = "yedam";
		//strArray[0] = "str"; 아래랑 같음
		strArray[0] = "yedam";
		//strArrat[0] == str == yedam; 삼단논법 p=q, q=r -> q=r
		
		strArray[1] = "yedam";
		strArray[2] = new String("yedam");
		
		System.out.println(strArray[0] == strArray[1]); // true 주소비교
		System.out.println(strArray[0] == strArray[2]); // false
		System.out.println(strArray[0].equals(strArray[2])); // true 데이터값 비교
	
		
		// for문 이용한 배열 복사
		int[] oldArray = {1,2,3};
		int[] newArray = new int[5];
	
		for(int i = 0; i<oldArray.length; i++) { // 원본기준
			newArray[i] = oldArray[i];
		}
	
		for(int i=0; i<newArray.length; i++) {
			System.out.println(newArray[i]);  // 1,2,3,0,0
		}
		
		
		int[] oldArray2 = {1,2,3,4,5,6,7};
		int[] newArray2 = new int[10];
		System.arraycopy(oldArray2, 0, newArray2, 0, oldArray2.length);
		//(원본배열, 몇번째부터, 넣을배열, 몇번째부터, 몇개복사?)
		
		for(int i=0; i<newArray2.length; i++) {
			System.out.println(newArray2[i]);  //1,2,3,4,5,6,7,0,0,0
		}
		
		// ==
		for(int temp : newArray2) {
			System.out.println(temp+"\t");
		}
		
		
	}
}
