package com.yedam.loop;

public class Exam04 {
	public static void main(String[] args) {
		
		// break문
		while(true) { //true=무한반복
			int num = (int) (Math.random()*6) + 1; //1~6
			System.out.println(num);
			if (num == 6) {
				break;
			}
		}
		System.out.println("end of prog");
		
		for(int i = 0; i <= 10; i++) {
			for(int j = 0; j <= 10; j++) {
				if(i+j == 4) {
					System.out.println("i + j = " + (i+j));
					break; // 중첩문에선 가까이 있는 반복문만 종료
					//System.out.println("안나갔다."); --> 브레이크때문에 실행 안됨
				}
			}
		}
	
	
	
	// break문 바깥 반복문 종료
	Outter : for(char upper = 'A'; upper <= 'z'; upper++) {
		for(char lower ='a'; lower <= 'z'; lower++) {
			System.out.println(upper + "-" + lower);
			if (lower == 'g') {
				break Outter;
			}
		}
	}
	System.out.println("end of prog");
	
	
	
	// Continue
	for(int i = 0; i <= 10; i++) {
		if(i % 2 == 0) {
			continue; // 짝수 나오면 continue = 아래로 내려가지 않고 위로 
			          // 짝수 출력X. 홀수만 출력
		}
		System.out.println(i);
	}
	
	
	
  }	
}
