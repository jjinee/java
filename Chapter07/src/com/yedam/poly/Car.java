package com.yedam.poly;

public class Car {
	// 필드
	// 필드의 다형성
	// 필드에 클래스 넣
	Tire frontLeftTire = new Tire("앞왼쪽", 6);
	Tire frontRightTire = new Tire("앞오른쪽", 2);
	Tire backLeftTire = new Tire("뒤왼쪽", 3);
	Tire backRightTire = new Tire("뒤오른쪽", 4);
			
	
	
	// 생성자
	
	// 메소드
	public int run() {
		System.out.println("자동차가 달립니다.");
		if(frontLeftTire.roll() == false) {
			stop();
			return 1; // 1 타이어 위치
		}
		if(frontRightTire.roll() == false) {
			stop();
			return 2;
		}
		if(backRightTire.roll() == false) {
			stop();
			return 3;
		}
		if(backLeftTire.roll() == false) {
			stop();
			return 4;
		}
		return 0; //펑크 안 난 경우
	}
	
	
	void stop() {
		System.out.println("자동차가 멈춥니다.");
	}
	
	
	
	
}
