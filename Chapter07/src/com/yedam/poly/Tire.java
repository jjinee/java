package com.yedam.poly;

public class Tire {
	//부모
	//필드
	public int maxRotation; // 최대 회전수(타이어 수명)
	public int accRotation; // 누적 회전수
	public String location; // 타이어 위치
	
	
	//생성자
	// 타이어 제작 순간 어디에 위치하는지, 수명 정해짐
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;
		
	}
	
	
	
	
	//메소드
	//타이어 굴러가는 메소드
	public boolean roll() { 
		++accRotation;
		if(accRotation < maxRotation) {
			System.out.println( location + "Tire 수명 : " + (maxRotation - accRotation) + "회");
			return true;
		}else {
			System.out.println("###" + location + "Tire 펑크" + "###");
			return false;
		}
	}
	
	
	
}
