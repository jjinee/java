package com.yedam.poly;

public class ChildExampld {
	public static void main(String[] args) {
		//Child child = new Child();
		
		
		// 클래스간의 자동타입변환
		
		// 부모 클래스에 있는 메소드만 사용하겠다
		// 단 자식 클래스에 재정의가 되어있으면
		// 자식 클래스에 재정의된 메소드를 사용
		//Parent parent = child; 
		// Parent 가 가지고 있는 내용을 parent가 실행 O
		// method 2는 자식에 재정의 o
		// -> method1,2만 실행
		
		//parent.method1();
		//parent.method2();
		//parent.method3();
		
		
		
		// 클래스간의 강제 타입 변환
		// 자동타입변환으로 인해서 자식클래스 내부에 정의된 필드, 메소드를 못 쓸 경우
		// 강제타입변환을 함으로써 자식 클래스 내부에 정의된 필드와 메소드를 사용
		Parent parent = new Child(); // 자동타입변환
		
		parent.field = "data1";
		parent.method1();
		parent.method2();
		
		//자식꺼 호출 -> 사용 x
		//parent.field2 = "date2";
		//parent.method3();
		
		//=> 강제타입변환 : 부모 + 자식것도 사용 가능
		Child child = (Child) parent; 
		child.field2 = "data2";
		child.method3();
		child.method1();
		child.method2();
		parent.field = "data1";
		
		//클래스 타입 확인 예제
		
		method1(new Parent()); // 자기자신으로 된 객체 만들어짐
		method1(new Child());
	
		
		GrandParent gp = new Child();
		// Child- Parent - GrandParent
		// 바로위 부모 아니더라도 상위타입이라면 자동타입변환 일어날수있음
		gp.method4();
	}
	
	//instanceof 객체타입확인
	public static void method1(Parent parent) {
		if(parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		} else {
			System.out.println("변환 실패");
		}
	}
	
	
}
