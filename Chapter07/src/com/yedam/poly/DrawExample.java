package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		// 자동타입변환
		// 부모클래스 변수 = new 자식클래스()
		
		//타입자리에 들어가있는 것만 사용가능 (Draw)
		Draw figure = new Circle();
		
		figure.x = 1;
		figure.y = 2;
		figure.draw(); // 부모것만 사용하지만 자식이 재정의한 것 있다면 자식거 실행
		
		
		
		
		// 다른애로 변수 초기화 Circle -> Rectangle
		// 하나의 부모(객체)로 여러 모습, 실행결과 나타냄 -> 다형성 : 타입변환, 메소드 오버라이딩
		figure = new Rectangle();
		figure.draw();
		
		
	}
}
