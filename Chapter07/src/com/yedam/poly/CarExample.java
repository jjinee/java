package com.yedam.poly;

public class CarExample {
	public static void main(String[] args) {
		Car car = new Car();
		
		// 자동차 5번 운행하겠다
		for(int i = 0; i < 5; i++) {
			int problemLoc = car.run();  //운행했을때 문제생기는 번호(바퀴위치) 알려줌
		switch (problemLoc) {
		// frontLeftTire
		case 1:
			System.out.println("앞 왼쪽 HankookTire 교환");
			// Tire = 부모믈래스 (슈퍼클래스)
			// HankookTire = 자식클래스(서브클래스_
			// Tire frontLeftTire = new HanKookTire("앞왼쪽", 15); =
			car.frontLeftTire = new HanKookTire("앞왼쪽", 15);
			// 왼쪽타이어 펑크나면 한국 타이어로 바꿈(한국타이어의 재정의된 내용)
			break;
			
		case 2:
			System.out.println("앞 오른쪽 KumhoTire 교환");
			car.frontRightTire = new KumhoTire("앞오른쪽", 15);
			break;
		case 3:
			System.out.println("뒤 왼쪽 HankookTire 교환");
			car.backLeftTire = new HanKookTire("뒤왼쪽", 15);
			break;
		case 4:
			System.out.println("앞 오른쪽 KumhoTire 교환");
			car.backRightTire = new KumhoTire("뒤오른쪽", 15);
			
			break;
		}
		System.out.println("=========================");
		}
		
		
		
		
	}
}
