package com.yedam.abs;

public class Cat extends Animal {

	public Cat() {
		this.kind = "포유류";
	}
	
	
	// 자식클래스 반드시 추상 메소드 재정의
	@Override
	public void sound() {
		System.out.println("야옹");
	}
}
