package com.yedam.inheri;

public class Person extends People {
	
	public int age;
	
	// 자식 객체 만들 때 생성자들 통해서 만든다.
	// super()를 통해서 부모 객체를 생성한다.
	// 여기서 super() 의미하는 것은 부모의 생성자를 호출
	// 따라서 자식객체 만들게 되면 부모 객체도 같이 만들진다
	public Person(String name, String ssn) { // 부모 생성자 기본생성자 아닐때 표기해줘야 함
		super(name, ssn); 
	}
}
