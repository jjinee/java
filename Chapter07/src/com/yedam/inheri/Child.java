package com.yedam.inheri;

public class Child extends Parent{
	public String lastName;
	public int age;
	
	
	// 메소드 오버라이딩
	
	@Override // 자바한테 오버라이딩 한 거 있다고 미리 알려주는 것
	public void method1() {
		System.out.println("Child class -> method1 Override");
	}

	@Override
	public void method2() {
		// TODO Auto-generated method stub
		super.method2();
	}

	public void method3() {
		System.out.println("Child class -> method3 Call");
	}
	
	
	
}
