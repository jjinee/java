package com.yedam.inheri;

public class OverrideExam {
	public static void main(String[] args) {
		Child child = new Child();
		
		
		child.method1(); // 부모꺼 오버라이딩한 자식꺼 출력
		child.method2(); // 부모
		child.method3(); // 자식
		
		
	}
}
