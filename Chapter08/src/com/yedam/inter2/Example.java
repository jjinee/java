package com.yedam.inter2;

public class Example {
	public static void main(String[] args) {
		ImplementC impl = new ImplementC();
		
		InterfaceA ia = impl;
		ia.methodA();
		
		System.out.println();
		
		InterfaceB ib = impl;
		ib.methodB();
		
		System.out.println();
		
		InterfaceC ic= impl; //A B 기능도 다 가지고 있음 A B의 자식 인터페이스
		ic.methodA();
		ic.methodB();
		ic.methodC();
		
	}
}
