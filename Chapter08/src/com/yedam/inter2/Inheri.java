package com.yedam.inter2;

public class Inheri {
	public static void main(String[] args) {
		// 자동타입변환
		// 손자 -> 부모
		// 부모 -> 조부모
		// => 손자 -> 조부모
		
		// A <- B <- D ( + A <-D)
		// A <- B
		A a = new B();
		a.info();
		
		
		// A <- D
		A a2 = new D();
		a2.info();
	}
}
