package com.yedam.inter2;

public class Application {
	public static void main(String[] args) {
	//매개변수의 다형성
	Vehicle v1 = new Bus();
//	drive(v1);
//	
//	Vehicle v2 = new Taxi();
//	drive(v2);

	v1.run();
	//v1.checkFare(); // Vehicl 에 정의된 건 run뿐.
	// checkFare 쓰고싶으면 강제 타입 변환
	
	
	Bus bus = (Bus) v1;
	
	bus.run();
	bus.checkFare();
	
	
	drive(new Bus());
	drive(new Taxi());
	
		
	}
	
		
	public static void drive(Vehicle vehicle) {
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		
		vehicle.run();
		
		
		
	}
}
