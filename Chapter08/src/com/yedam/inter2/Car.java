package com.yedam.inter2;

public class Car {
	Tire frontLeftTire = new HanKookTire();
	Tire frontRighttTire = new KumhoTire();
	Tire BackLeftTire = new HanKookTire();
	Tire BackRighttTire = new KumhoTire();
	
	public void run() {
		frontLeftTire.roll();
		frontRighttTire.roll();
		BackLeftTire.roll();
		BackRighttTire.roll();
	}
	
}
