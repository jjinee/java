package com.yedam.Inter;

public interface WashingMachine extends DryCourse{ //세탁기에 건조기능 추가
	// 인터페이스에 인터페이스 상속
	public void startBtn(); // 시작버튼
	public void pauseBtn(); // 일시정지
	public void stopBtn();
	public int changeSpeed(int speed);
	
}
