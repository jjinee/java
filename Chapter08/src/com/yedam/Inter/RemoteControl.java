package com.yedam.Inter;

public interface RemoteControl extends Searchable {
	// 인터페이스 인터페이스 상속 가능
	
	// 인터페이스 : 상수, 추상메소드로만 이루어짐 스스로 객체 못만듦
	
	
	// 상수
	public static final int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0;  //static final 생략. (항상 들어가 있음)
	
	
	// 추상 메소드
	public void turnOn();
	public abstract void turnOff(); // abstract 생략 가능
	public void setVolume(int volume);
}
