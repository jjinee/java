package com.yedam.Inter;

public class MyClass {
	// 클래스 내부에 인터페이스 사용
	
	// 필드
	// 1) 필드에 인터페이스 사용
	RemoteControl rc = new Television(); // RemoteContro 인터페이스를 사용하고 텔레비전에 재정의한 내용 사용
	
	// 생성자
	
	
	public MyClass() {
		
	}
	
	// 2) 생성자 매개변수에 인터페이스 사용
	public MyClass(RemoteControl rc) {
		this.rc = rc;
		rc.turnOn();
		rc.turnOff();
				
	}
	
	// 메소드
	// 3) 메소드 안에서 인터페이스를 로컬변수로 사용. 원하는 자식 클래스 넣고 그거 사용
	public void method1() {
		RemoteControl rtc = new Audio();
		rtc.turnOn();
		rtc.setVolume(5);
	}
	
	// 4) 메소드 매개변수로 사용
	public void methodB (RemoteControl rtc) {
		rtc.turnOn();
		rtc.turnOff();
		
	}
			
			
}
