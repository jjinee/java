package com.yedam.Inter;

public class Circle implements GetInfo {

	// 필드
	int radius; 
	
	// 생성자
	public Circle(int radius) {
		this.radius = radius;
	}
	
	
	@Override
	public void area() {
		//원 넓이 PI * R * R
		System.out.println(3.14 * radius * radius);
	}	

	@Override
	public void round() {
		//원 둘레 2 * PI * R
		System.out.println(2 * Math.PI * radius);
	}

}
