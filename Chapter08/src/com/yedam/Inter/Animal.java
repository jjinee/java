package com.yedam.Inter;

public interface Animal {
	
	void walk();
	void fly();
	void sing();
}
