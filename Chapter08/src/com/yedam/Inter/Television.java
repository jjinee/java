package com.yedam.Inter;

public class Television implements RemoteControl{
	// TV소리 조절 리모컨 만들기
	
	// 필드
	private int volume; // 볼륨 정보 받을 수 있는 필드
	
	
	// 생성자
	
	
	
	// 메소드
	
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
	}
	

	@Override
	public void setVolume(int volume) {
		//0보다 작은, 10보다 큰 볼륨 없음
		if(volume > RemoteControl.MAX_VOLUME) { // 상수 가져오는 법
			this.volume = RemoteControl.MAX_VOLUME; // 최대볼륨보더 큰 값 없음
		} else if (volume < RemoteControl.MIN_VOLUME) { // 최소소리 이하 데이터 들어올 때 
			this.volume = RemoteControl.MIN_VOLUME;
		} else { // 정상 범주
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨 : " + this.volume);
	}

	@Override
	public void search(String url) {
		
	}
	
	
	
	
}
