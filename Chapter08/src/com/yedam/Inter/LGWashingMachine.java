package com.yedam.Inter;

public class LGWashingMachine implements WashingMachine {// 세탁+건조
	//세탁기에 건조기능 추가

	
	// DryCourse
	
	@Override
	public void dry() {
		System.out.println("건조 코스 진행");
	}

	
	
	//WashingMachine
	
	@Override
	public void startBtn() {
		System.out.println("빨래 시작");
	}

	@Override
	public void pauseBtn() {
		System.out.println("빨래 일시 중시");
	}

	@Override
	public void stopBtn() {
		System.out.println("빨래 중지");
	}

	@Override
	public int changeSpeed(int speed) {
		int nowSpeed = 0;
		switch (speed) {
		case 1:
			nowSpeed = 20;
			break;
		case 2:
			nowSpeed = 50;
			break;
		case 3:
			nowSpeed = 100;
			break;
		}
		
		return nowSpeed;
	} 
	
	
	
	
	
}
