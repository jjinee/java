package com.yedam.Inter;

public class RCExample {
	public static void main(String[] args) {
		// 자동타입변환과 오버라이딩 통한 다형성
		// 자동타입변환 : 부모꺼 + 자식에 메소드에 재정의 된 것
		RemoteControl rc; 
		
		rc = new SmartTV();
		
		//SmartTV 클래스 - > implements RemoteCOntrol ( +Searchable)
		// RemoteCOntrol(+ Searchable) -> Searchable 상속 받고 있기 떄문에
		//RemoteControl에 있는 기능 
		rc.turnOn();
		rc.setVolume(40);
		rc.turnOff();
		
		//Sarch에 있는 기능
		rc.search("www.google.com"); 
		
//		rc = new Audio();
//		
//		rc.turnOn();
//		rc.setVolume(5);
//		rc.turnOff();
		
		
		
		
		// 인터페이스 x 텔레비전 자신을 객체로 만듦 
//		Television tv = new Television();
//		
//		tv.turnOn();
//		tv.setVolume(4);
//		tv.turnOff();
		
		
	}
}
