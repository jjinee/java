package com.yedam.Inter;

public class MyClassExample {
	public static void main(String[] args) {
		System.out.println("1)===============");
		
		
		MyClass myClass = new MyClass();
		 
		myClass.rc.turnOn();  //rc라는 객체가 가지고있는 메소드 실행 : 객체담고있는 클래스(MyClass) 인스턴스화 -> 품고있는 필드 rc 호출
		myClass.rc.turnOff();
		
		System.out.println();
		System.out.println("2)===============");
	
		//3) 생성자 매개변수에 인터페이스 사용
		MyClass myClass2 = new MyClass(new Audio());
		// 매개 변수로 자식 클래스 넘겨줌. 오디오가 가진 매소드 사용
		// 객체 만들어지면서 안에 생성자 실행
	
		System.out.println();
		System.out.println("3)===============");
		
		// 메소드 안에서 인터페이스를 로컬변수로 사용
		MyClass myClass3 = new MyClass();
		myClass3.method1();
		
		System.out.println();
		System.out.println("4)===============");
		
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television());
		//생성자 호출이랑 비슷. 생성자냐 메소드냐 차이
		// RemoteControl에 정의된 rtc를 텔레비전이 재정의한
		
		
		
		// ★☆ 개발코드를 수정하지 않으면서 객체(자식클래스) 교환이 가능
		// => 로직 건들지 않고 안에 자식클래스만 바꾸면 객체 바뀜
		// 인터스페이스 스스로 객체 안되기때문에 자식 클래스와 통신하면서
		// 자식클래스에 재정의된 메소드 받아와 호출 출력
		// 인터스페이스는 중간다리 역할 ☆★
	
	}
}
