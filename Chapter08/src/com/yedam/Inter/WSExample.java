package com.yedam.Inter;

public class WSExample {
	public static void main(String[] args) {
		
		// 한객체에 두가지 인터페이스 사용 ( WashingMachine 인터페이스에 DryCourse 인터페이스 상속)
		WashingMachine LGws = new LGWashingMachine();
		
		//WashingMachine 
		LGws.startBtn();
		LGws.pauseBtn();
		LGws.changeSpeed(2);
		System.out.println("세탁기 속도 : " + LGws.changeSpeed(2) + " 변경하였습니다.");
		LGws.startBtn();
		
		// DryCourse
		LGws.dry();
	}
}
