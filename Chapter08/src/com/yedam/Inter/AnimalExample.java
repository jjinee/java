package com.yedam.Inter;

public class AnimalExample {
	public static void main(String[] args) {
		Animal bird = new Bird();
		
		// 애니멀에 정의된 추상메소드 재정의한 버드 클래스에서 가져옴
		bird.walk();
		bird.fly();
		bird.sing();
	}
}
