package com.yedam.study;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		
		// 두 개의 주사위를 던졌을 때, 눈의 합이 6이 되는 모든 경우의 수를
		// 출력하는 프로그램을 구현 하시오.
		// 1, 5
		// 2, 4
		// 3, 3
		// 4, 2
		// 5, 1
		// A주사위 B주사위
		// A주사위가 1일 때, B주사위가 1~6까지의 합을 구한 다음
		// 합의 결과가 6이면 내가 원하는 조건 만족
		// 1 - 1 2 3 4 5 6
		// 2- 1 2 3 4 5 6
		
		for(int i = 1; i <= 6; i++) {
			for (int j = 1; j<=6; j++) {
				if(i+j==6) {
					System.out.println(i +" + " + j + " = " + (i+j)); 
				}				
			}						
		}
		
		
		
		
		// 숫자 하나를 입력 받아, 양수인지 음수인지 출력
		// 단 0이면 0입니다 라고 출력
		
		Scanner sc = new Scanner(System.in);
		System.out.println("숫자를 입력하세요>");
		int num = Integer.parseInt(sc.nextLine());
		if(num > 0) {
			System.out.println("양수입니다");
		} else if (num < 0) {
			System.out.println("음수입니다");
		} else{
			System.out.println("0입니다");
		}
		
		
		
		// 정수 두개와 연산기호 1개를 입력 받아서
		// 연산 기호에 해당되는 계산을 수행하고 출력하세요
			
		System.out.println("숫자를 입력하세요>");
		int num1 = Integer.parseInt(sc.nextLine());
		System.out.println("숫자를 입력하세요>");
		int num2 = Integer.parseInt(sc.nextLine());
		System.out.println("연산기호를 입력하세요 >");
		String str = sc.nextLine();
				
		if(str.equals("+")) {
			System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
		} else {
			System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
		}
			
			
		
	}
}
