package homework;

import java.util.Scanner;

public class homework_loof {
 public static void main(String[] args) {
	 Scanner scanner = new Scanner(System.in);

		// 문제1) 차례대로 x와 y의 값이 주어졌을 때 어느 사분면에 해당되는지 출력하도록 구현하세요.
		// 각 사분면에 해당 하는 x와 y의 값은 아래를 참조하세요.
		// 제1사분면 : x>0, y>0
		// 제2사분면 : x<0, y>0
		// 제3사분면 : x<0, y<0
		// 제4사분면 : x>0, y<0
		// 문제출처, 백준(https://www.acmicpc.net/) 14681번 문제
		
	    System.out.println("-----사분면 확인-----");
	    System.out.println("x값");
	    System.out.println("입력>");
	 	int x = scanner.nextInt();
	 	System.out.println("y값");
	    System.out.println("입력>");
	 	int y = scanner.nextInt();
	 	
	 	if(x > 0 && y > 0) {
	 		System.out.println("제1사분면 입니다.");
	 	} else if(x < 0 && y > 0) {
	 		System.out.println("제2사분면 입니다.");
	 	} else if(x < 0 && y < 0) { 
	 		System.out.println("제3사분면 입니다.");
	 	} else {
	 		System.out.println("제4사분면 입니다.");
	    }
	 	
	 	
	 	// 문제2) 연도가 주어졌을 때 해당 년도가 윤년인지를 확인해서 출력하도록 하세요.
		// 윤년은 연도가 4의 배수이면서 100의 배수가 아닐 때 또는 400의 배수일때입니다.
		// 예를 들어, 2012년은 4의 배수이면서 100의 배수가 아니라서 윤년이며,
		// 1900년은 100의 배수이고 400의 배수는 아니기 때문에 윤년이 아닙니다.
		// HiNT : 이중 IF문 사용
		// 문제출처, 백준(https://www.acmicpc.net/) 2753번 문제
	 
	 	System.out.println("-----윤년 확인-----");
	 	System.out.println("년도를 입력하세요.");
	 	System.out.println("입력>");
	 	int year = scanner.nextInt();
	 	
	 	if(year%4 == 0 && year%100 !=0 || year%400 == 0) {
	 		System.out.println("윤년입니다.");
	 	} else {
	 		System.out.println("윤년이 아닙니다.");
	 	}
	 	
	 	
	 
		
		// 문제3) 반복문을 활용하여 up & down 게임을 작성하시오. 1~100사이
		// 기회는 5번 이내로 맞추도록 하며, 맞추게 될 시에는 정답 공개 및 축하합니다.
		// 메세지 출력.
		// 사용자가 답안 제출 시, up, down이라는 메세지를 출력하면서 
		// 정답 유추를 할 수 있도록 한다.
	 
	 	int randomNo = (int)(Math.random() * 100) + 1;
	 	int tryNo = 0;
	 	
	 	while (tryNo < 5) {
	 		tryNo++;
	 		
	 		System.out.println("-----Up & Down 게임-----");
		 	System.out.println("숫자를 입력하세요.");
		 	System.out.println("입력>");
		 	int updown = scanner.nextInt();
		 	
		 	if (updown < randomNo) {
		 		System.out.println("up");
		 	} else if (updown > randomNo) {
		 		System.out.println("down");
		 	} else {
		 		System.out.println("정답 : " + updown + " ! 축하합니다 !");
		 		break; 
		 	}
	 	}
	 
	 
		// 문제4) 차례대로 m과 n을 입력받아 m단을 n번째까지 출력하도록 하세요.
		// 예를 들어 2와 3을 입력받았을 경우 아래처럼 출력합니다.
		// 2 X 1 = 2
		// 2 X 2 = 4
		// 2 X 3 = 6

	 	
	    System.out.println("-----구구단-----");
	    System.out.println("m");
	    System.out.println("입력>");
	 	int m = scanner.nextInt();
	 	System.out.println("n");
	    System.out.println("입력>");
	 	int n = scanner.nextInt();
	 
	 	
	 	
	 	
	 	
	 	
	}

}
